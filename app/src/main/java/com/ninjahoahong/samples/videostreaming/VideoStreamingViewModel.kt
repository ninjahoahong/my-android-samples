package com.ninjahoahong.samples.videostreaming

import androidx.lifecycle.ViewModel
import com.google.android.exoplayer2.ExoPlayer

class VideoStreamingViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    private var player: ExoPlayer? = null
    private var playbackPosition: Long = 0
    private var currentWindow: Int = 0
    private var playWhenReady: Boolean = true
}
