package com.ninjahoahong.samples.videostreaming

import android.net.Uri
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory

class MediaSourceBuilder {

    //Build various MediaSource depending upon the type of Media for a given video/audio uri
    fun build(uri: Uri): MediaSource {
        val userAgent = PlayerConstants.USER_AGENT
        val lastPath = uri.lastPathSegment ?: ""

        val defaultHttpDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)

        return if (lastPath.contains(PlayerConstants.FORMAT_MP3) || lastPath.contains(
                PlayerConstants.FORMAT_MP4
            )) {
            ProgressiveMediaSource.Factory(defaultHttpDataSourceFactory)
                .createMediaSource(uri)
        } else {
            HlsMediaSource.Factory(defaultHttpDataSourceFactory)
                .createMediaSource(uri)
        }
    }


    //Overloaded function to Build various MediaSource for whole playlist of video/audio uri
    fun build(uriList: Array<Uri>): MediaSource {
        val playlistMediaSource = ConcatenatingMediaSource()
        uriList.forEach { playlistMediaSource.addMediaSource(build(it)) }
        return playlistMediaSource
    }

}