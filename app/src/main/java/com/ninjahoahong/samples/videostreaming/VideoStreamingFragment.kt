package com.ninjahoahong.samples.videostreaming

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.ninjahoahong.samples.R
import kotlinx.android.synthetic.main.fragment_video_streaming.playerView

class VideoStreamingFragment : Fragment() {

//    private lateinit var viewModel: VideoStreamingViewModel
    private lateinit var viewModel: PlayerViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_video_streaming, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProviders.of(this).get(VideoStreamingViewModel::class.java)
//        // TODO: Use the ViewModel
        viewModel = ViewModelProviders.of(this).get(PlayerViewModel::class.java)
        viewModel.player.observe(this, Observer {
            playerView.player = it
        })
    }

}
