package com.ninjahoahong.samples.coroutines

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RetrofitWithCoroutinesViewModel : ViewModel() {

    val repository: TodoRepository = TodoRepository()

    val currentTodo: LiveData<Todo> = liveData {
        emit(repository.getTodo(5))
    }

//    var currentTodo: Todo? = null

//    fun setCurrentTodo(id: Int) {

//        viewModelScope.launch {
//            Log.d("RetrofitWithCoroutinesViewModel", "launch: $id")
//            withContext(Dispatchers.IO) {
//                val itemTodo = repository.getTodo(id).execute().body()
//                Log.d("RetrofitWithCoroutinesViewModel", "withContext")
//                currentTodo.value = itemTodo
//            }
//        }
//        var itemTodo: Todo? = null
//        viewModelScope.launch {
//            Log.d("RetrofitWithCoroutinesViewModel", "launch: $id")
//            itemTodo = repository.getTodo(id)
//            Log.d("RetrofitWithCoroutinesViewModel", "itemTodo set: $id")
//        }
//        currentTodo.value = itemTodo
//        Log.d("RetrofitWithCoroutinesViewModel", "currentTodo: $id")
//        viewModelScope.launch {
//            withContext(Dispatchers.IO) {
//                itemTodo = repository.getTodo(id)
//            }
//        }
//        currentTodo.value = itemTodo

//        CoroutineScope(Dispatchers.IO).launch {
//            val itemTodo = repository.getTodo(id)
//            withContext(Dispatchers.Main) {
//                currentTodo.value = itemTodo
//            }
//        }
//        viewModelScope.launch {
//            Log.d("RetrofitWithCoroutinesViewModel", "launch: $id")
//            getTodoId(id)
//        }
//    }

//    private suspend fun getTodoId(id: Int) = withContext(Dispatchers.IO) {
//        currentTodo = repository.getTodo(id)
//    }
}
