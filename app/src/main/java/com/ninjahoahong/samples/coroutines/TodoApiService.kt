package com.ninjahoahong.samples.coroutines

import retrofit2.http.GET
import retrofit2.http.Path

interface TodoApiService {
    @GET("/todos/{id}")
    suspend fun getTodo(@Path("id") todoId: Int): Todo

//    @GET("/todos/{id}")
//    fun getTodo(@Path("id") todoId: Int): Call<Todo>
}