package com.ninjahoahong.samples.coroutines

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TodoRepository {
    val client: TodoApiService = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
        .create(TodoApiService::class.java)

    suspend fun getTodo(id: Int) = client.getTodo(id)
}