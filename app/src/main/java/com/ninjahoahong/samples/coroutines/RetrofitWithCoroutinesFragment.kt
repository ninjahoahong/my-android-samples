package com.ninjahoahong.samples.coroutines

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.liveData
import com.google.gson.GsonBuilder
import com.ninjahoahong.samples.R
import kotlinx.android.synthetic.main.fragment_retrofit_with_coroutines.todoTitle
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitWithCoroutinesFragment : Fragment() {

    companion object {
        fun newInstance() = RetrofitWithCoroutinesFragment()
    }

    //    val client: TodoApiService = Retrofit.Builder()
//        .baseUrl("https://jsonplaceholder.typicode.com/")
//        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
//        .build()
//        .create(TodoApiService::class.java)
//    private lateinit var viewModel: RetrofitWithCoroutinesViewModel
    private val viewModel: RetrofitWithCoroutinesViewModel by viewModels()
//    val client: TodoApiService = Retrofit.Builder()
//        .baseUrl("https://jsonplaceholder.typicode.com/")
//        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
//        .build()
//        .create(TodoApiService::class.java)
    val observer = Observer { todo: Todo ->
        Log.d("observer", "todo ${todo.title}")
        todoTitle.text = todo.title
    }
//    val todoItem: LiveData<Todo> = liveData {
//        emit(client.getTodo(5))
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_retrofit_with_coroutines, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        viewModel = ViewModelProviders.of(this).get(RetrofitWithCoroutinesViewModel::class.java)
        viewModel.currentTodo.observeForever(observer)
//
//        viewModel.setCurrentTodo(5)
//
//        viewModel.currentTodo.observeForever(Observer { todoItem ->
//            if (todoItem != null) {
//                todoTitle.text = todoItem.title
//            }
//        })
//        if (viewModel.currentTodo != null) {
//            todoTitle.text = viewModel.currentTodo!!.title
//        }
//
//        CoroutineScope(Dispatchers.IO).launch {
//            Log.d("RetrofitWithCoroutinesFragment", "client ${Thread.currentThread().name}")
//            val todoItem = client.getTodo(5)
//
//            withContext(Dispatchers.Main) {
//                Log.d("RetrofitWithCoroutinesFragment", "withContext ${Thread.currentThread().name}")
//                todoTitle.text = todoItem.title
//            }
//        }


//        todoItem.observe(viewLifecycleOwner, observer)


//        CoroutineScope(Dispatchers.IO).launch {
//            Log.d("Retrofit", "launch")
//            client.getTodo(3).runCatching { todoTitle.text = title }
//            withContext(Dispatchers.Main) {
//                Log.d("Retrofit", "withContext")
//                try {
//                    Log.d("Retrofit", "setTitle")
//                    todoTitle.text = itemTodo.title
//                } catch (e: HttpException) {
//                    Log.e("Retrofit", e.message)
//                    e.printStackTrace()
//                } catch (e: Throwable) {
//                    Log.e("Retrofit", e.message)
//                }
//            }
//        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.currentTodo.removeObserver(observer)
    }

}
